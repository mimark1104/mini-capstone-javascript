const textElement = document.getElementById('text')
const optionButtonsElement = document.getElementById('option-buttons')

let state = {}

function startGame() {
  state = {}
  showTextNode(1)
}

function showTextNode(textNodeIndex) {
  const textNode = textNodes.find(textNode => textNode.id === textNodeIndex)
  textElement.innerText = textNode.text
  while (optionButtonsElement.firstChild) {
    optionButtonsElement.removeChild(optionButtonsElement.firstChild)
  }

  textNode.options.forEach(option => {
    if (showOption(option)) {
      const button = document.createElement('button')
      button.innerText = option.text
      button.classList.add('btn')
      button.addEventListener('click', () => selectOption(option))
      optionButtonsElement.appendChild(button)
    }
  })
}

function showOption(option) {
  return option.requiredState == null || option.requiredState(state)
}

function selectOption(option) {
  const nextTextNodeId = option.nextText
  if (nextTextNodeId <= 0) {
    return startGame()
  }
  state = Object.assign(state, option.setState)
  showTextNode(nextTextNodeId)
}

const textNodes = [
  {
    id: 1,
    text: 'Hi, Ako nga pala si DingDong. Pwede ba makipag kaibigan?',
    options: [
      {
        text: 'Oo naman! Bakit hindi? pero kaibigan lang ba?',
        setState: { x: true },
        nextText: 2
      },
      {
        text: 'tekaaaaa! ayaw ko maging kabet moooooo.',
        nextText: 11
      }
    ]
  },
  {
    id: 2,
    text: 'Gusto sana kita makilala pa ng buong buo eh. ano ba gusto mo sa buhay?',
    options: [
      {
        text: 'netflix and chill.',
        requiredState: (currentState) => currentState.x,
        setState: { x: false, y: true },
        nextText: 3
      },
      {
        text: 'ikaw gusto ko eh. Tamang Kape lang.',
        requiredState: (currentState) => currentState.x,
        setState: { x: false, z: true },
        nextText: 3
      },
      {
        text: 'ayoko sana pero gusto ko hentai with senpai.',
        nextText: 3
      }
    ]
  },
  {
    id: 3,
    text: 'Nice! Sobrang Nice naman ng mga gusto mo haaaaa. napapaisip ako kung ikaw na ba talaga eh. San mo gusto pumunta ngayon? hanapin ko lang ferrari ko ha.',
    options: [
      {
        text: 'Explore Manila',
        nextText: 4
      },
      {
        text: 'Find a room and rest.',
        nextText: 5
      },
      {
        text: 'gusto ko pang maglibot libot hanggang antipolo eh',
        nextText: 6
      }
    ]
  },
  {
    id: 4,
    text: 'You are so tired that you fall asleep while exploring manila kaso mukhang ako din napagod hehehe.',
    options: [
      {
        text: 'natulog ng mahimbing eh, restart',
        nextText: -1
      }
    ]
  },
  {
    id: 5,
    text: 'haaaaay sobrang pagod na ako *tumunog ang phone at tumawag si mariane* "bebi, san ka? kasama mo na ba pinsan mo? ',
    options: [
      {
        text: 'malandi ka kaso mas malandi ang asawa, restart',
        nextText: -1
      }
    ]
  },
  {
    id: 6,
    text: 'Habang nililibot ang antipolo gamit naman ang ducatti.',
    options: [
      {
        text: 'sabihan si dingdong na iexplore pa lalo ang buong antipolo',
        nextText: 7
      }
    ]
  },
  {
    id: 7,
    text: 'While exploring antipolo biglang tumawag si mariane',
    options: [
      {
        text: 'Try to ignore',
        nextText: 8
      },
      {
        text: 'Attack dingdong with your lips',
        requiredState: (currentState) => currentState.y,
        nextText: 9
      },
      {
        text: 'Hug dingdong behind',
        requiredState: (currentState) => currentState.z,
        nextText: 10
      },
      {
        text: 'Throw the blue and layuan si DingDong',
        requiredState: (currentState) => currentState.x,
        nextText: 11
      }
    ]
  },
  {
    id: 8,
    text: 'Your attempts to ignore the reality naging malandi ka',
    options: [
      {
        text: 'Restart, landi eh pero good job',
        nextText: -1
      }
    ]
  },
  {
    id: 9,
    text: 'You foolishly thought this dingdong make you special.',
    options: [
      {
        text: 'Restart hahahha wawa kasi malandi',
        nextText: -1
      }
    ]
  },
  {
    id: 10,
    text: 'dingdong laughed at you and leave you bigla.',
    options: [
      {
        text: 'Restart kasi iyak na iyak ka na',
        nextText: -1
      }
    ]
  },
  {
    id: 11,
    text: 'Buti naisip mo yun at hindi ka naging kabit!',
    options: [
      {
        text: 'Congratulations di ka malandi eh pero 4ever single ka. Play Again.',
        nextText: -1
      }
    ]
  }
]

startGame()